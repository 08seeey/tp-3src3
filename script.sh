#!/bin/bash

# Copier le fichier de configuration sur le serveur

cp config_files/ssh/sshd_config /etc/ssh/sshd_config

cp config_files/ssh/Banner /etc/Banner

# On créer l'utilisateur avec la commande adduser
printf "Indiquer le nom de l'utilisateur : \n"
read username
useradd $username

# On donne les droits sudo à l'utilisateur
usermod -a -G sudo $username

# On configure un mot de passe pour l'utilisateur
printf "Indiquer le mot de passe : \n"
password $username

# On configure la clé ssh publique
printf "Entrer une clé publique : \n"
read clessh
echo $clessh >> ~/.ssh/authorized_keys

# Redemarrer le service sshd 
systemctl restart sshd 
